# browser-upgrade-page

Just a simple (friendly) way to show the visitors they have an outdated &/or unsupported browser, because it may/may not be compatible with the site.

It will redirect the visitors to a custom page (`/upgrade/index.php`), where they will be told to upgrade their browser. A list of a few browser are presented below.

By default, _all_ MSIE browsers are redirected. Please, edit the `.htaccess` file in case you still would like to support one - like IE 11, perhaps. Versions of Safari less than 9.1 are also redirected. _Perhaps a bit overkill - when using a HTTPS site with TLSv1.2, it will remove most old browsers anyway._


## Demo

You can try it on my site: [iefdev.se][demo]. Change your “User Agent” to something old'ish.


## Installation:

Open `/upgrade/index.php` and change any text, if you want/need to.

Then upload the folder to your web root.

Edit your `.htaccess` file (...if you have one)

```apache
# Using IE=edge & chrome=1 in the example
<IfModule mod_setenvif.c>
	<IfModule mod_headers.c>
		BrowserMatch MSIE ie
		Header set X-UA-Compatible "IE=edge,chrome=1" env=ie
	</IfModule>
</IfModule>
<IfModule mod_headers.c>
	Header append Vary User-Agent
</IfModule>

# No MSIE, Safari 9.1+
# Edit to match your needs.
RewriteEngine on
#RewriteCond %{HTTP_USER_AGENT} MSIE\ [5-9] [NC,OR]
#RewriteCond %{HTTP_USER_AGENT} MSIE\ [10] [NC,OR]
RewriteCond %{HTTP_USER_AGENT} .*(MSIE).* [NC,OR]
RewriteCond %{HTTP_USER_AGENT} (Macintosh).*(Version/[3-8]).*(Safari) [NC,OR]
RewriteCond %{HTTP_USER_AGENT} (Macintosh).*(Version/9.0).*(Safari)
RewriteCond %{REQUEST_URI} !/upgrade/
RewriteRule ^(.*)$ /upgrade/ [L]
```

If you don't have an `.htaccess` file already? Upload the included file (`_.htaccess`) to your server, and rename it:

	_.htaccess -> .htaccess


## Extended usage

The upgrade page is kind of generic now, to be able to use it as a fallback page with other scripts, like the js-example I added in the “dummy page”. To read more about the javascript files - see [Files.md][files]


## Screenshot

![][scrap]


## @todo

- [ ] Add examples how to extend the usage.
- [ ] Try/test and include an example I saw, with `SSLVerifyClient`… [»»»][ssl]. So instead of getting a white page, or an error - they'll get the upgrade page instead.


## Contributing

1. Fork it (<https://gitlab.com/iEFdev/browser-upgrade-page/forks/new>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Merge Request


<!-- Markdown: link & image dfn's -->
[demo]: https://iefdev.se "Demo"
[scrap]: https://gitlab.com/iEFdev/browser-upgrade-page/raw/master/Screenshot.jpg
[files]: https://gitlab.com/iEFdev/browser-upgrade-page/blob/master/Files.md
[ssl]: http://www.askapache.com/htaccess/apache-ssl-in-htaccess-examples/#Politely_requiring_SSL_client_authentication