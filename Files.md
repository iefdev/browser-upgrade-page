# About the js-files


#### `detect_support.js`

A javascript/function to check to specific CSS support… Edit the script to match your needs and add it to the site/page that needs to be tested.

Default example: CSS Variables:

```js
(function detectSupport() {
    var upradeURL = '/upgrade',

		/**
         * List your CSS @support tests.
         * On the upgrade page, it will add to console.log():
         *      Your browser doesn't support “Foo Bar Variables”: @supports ($foo, Bar)
         */
        cssObj = {
            'CSS Variables': ['--var', 0],
            //'Foo Bar Variables': ['$foo', 'bar'],
        };

		// ...
```

_To see it work, uncomment the Foo Bar example._


- - -

#### `detect_support.min.js`

Is just an example file of the script minified.

```bash
$ uglifyjs detect_support.js -o detect_support.min.js -c -m
```

- - -

#### `upgrade.js`

This script is used by `/upgrade/index.php`. It will read the `sessionStorage.getItem()` _(if there is one)_, and put the message from `detect_support.js` in the `console.log()`, and then remove it.