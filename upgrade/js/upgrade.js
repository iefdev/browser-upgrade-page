/**
 * Adding a message to console.log() and
 * removing the item in sessionStorage
 */

// If using detect_support.js
if (sessionStorage.getItem('detect_support.js')) {
    var consoleText = sessionStorage.getItem('detect_support.js');
    console.log(consoleText);
    sessionStorage.removeItem('detect_support.js');
}
